package ru.homelab.springboothibernaterestjwt.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthorDto {
    @NotBlank
    private String firstname;
    @NotBlank
    private String lastname;
}

package ru.homelab.springboothibernaterestjwt.error;

public class BookNotFountException extends RuntimeException {

    public BookNotFountException(Long id) {
        super(String.valueOf(id));
    }
}

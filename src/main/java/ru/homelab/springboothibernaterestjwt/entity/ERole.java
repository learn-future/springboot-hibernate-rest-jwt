package ru.homelab.springboothibernaterestjwt.entity;

public enum ERole {
    ROLE_USER, ROLE_MODERATOR, ROLE_ADMIN
}

package ru.homelab.springboothibernaterestjwt.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "authors")
public class Author {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    @Column(length = 15)
    private String firstname;

    @NotBlank
    @Column(length = 15)
    private String lastname;

    @ManyToMany(mappedBy = "authors")
    @JsonIgnore
    private List<Book> books;
}

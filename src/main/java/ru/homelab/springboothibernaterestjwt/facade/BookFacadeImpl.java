package ru.homelab.springboothibernaterestjwt.facade;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.homelab.springboothibernaterestjwt.dto.BookDto;
import ru.homelab.springboothibernaterestjwt.entity.Book;
import ru.homelab.springboothibernaterestjwt.mapper.BookMapper;
import ru.homelab.springboothibernaterestjwt.service.BookService;

import java.util.List;

@Component
@RequiredArgsConstructor
public class BookFacadeImpl implements BookFacade {
    private final BookService bookService;
    private final BookMapper bookMapper;

    @Override
    public List<BookDto> findAll() {
        return bookService.findAll().stream()
                .map(bookMapper::mapToBookDto)
                .toList();
    }

    @Override
    public BookDto findById(Long id) {
        return bookMapper.mapToBookDto(bookService.findById(id));
    }

    @Override
    public BookDto create(BookDto bookDto) {
        Book book = bookMapper.mapToBook(bookDto);
        return bookMapper.mapToBookDto(bookService.create(book));
    }

    @Override
    public BookDto update(Long id, BookDto bookDto) {
        Book book = bookMapper.mapToBook(bookDto);
        book.setId(id);
        return bookMapper.mapToBookDto(bookService.update(book));
    }

    @Override
    public void delete(Long id) {
        bookService.delete(id);
    }
}

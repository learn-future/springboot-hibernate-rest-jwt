package ru.homelab.springboothibernaterestjwt.facade;

import ru.homelab.springboothibernaterestjwt.dto.BookDto;

import java.util.List;

public interface BookFacade {
    List<BookDto> findAll();

    BookDto findById(Long id);

    BookDto create(BookDto bookDto);

    BookDto update(Long id, BookDto bookDto);

    void delete(Long id);
}

package ru.homelab.springboothibernaterestjwt.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.homelab.springboothibernaterestjwt.entity.Book;
import ru.homelab.springboothibernaterestjwt.error.BookNotFountException;
import ru.homelab.springboothibernaterestjwt.repository.BookRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {
    private final BookRepository bookRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Book> findAll() {
        return bookRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Book findById(Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new BookNotFountException(id));
    }

    @Override
    @Transactional
    public Book create(Book book) {
        return bookRepository.save(book);
    }

    @Override
    @Transactional
    public Book update(Book book) {
        Book existingBook = bookRepository.findById(book.getId()).orElseThrow(() -> new BookNotFountException(book.getId()));
        existingBook.setTitle(book.getTitle());
        existingBook.setYear(book.getYear());
        existingBook.setAuthors(book.getAuthors());
        return bookRepository.save(existingBook);
    }

    @Override
    @Transactional
    public void delete(Long id) {
        bookRepository.findById(id).orElseThrow(() -> new BookNotFountException(id));
        bookRepository.deleteById(id);
    }
}

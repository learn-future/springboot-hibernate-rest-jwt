package ru.homelab.springboothibernaterestjwt.service;

import ru.homelab.springboothibernaterestjwt.entity.Book;

import java.util.List;

public interface BookService {
    List<Book> findAll();

    Book findById(Long id);

    Book create(Book book);

    Book update(Book book);

    void delete(Long id);
}

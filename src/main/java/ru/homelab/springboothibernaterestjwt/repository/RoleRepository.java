package ru.homelab.springboothibernaterestjwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.homelab.springboothibernaterestjwt.entity.ERole;
import ru.homelab.springboothibernaterestjwt.entity.Role;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(ERole name);
}

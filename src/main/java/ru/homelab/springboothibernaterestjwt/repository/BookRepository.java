package ru.homelab.springboothibernaterestjwt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.homelab.springboothibernaterestjwt.entity.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
}

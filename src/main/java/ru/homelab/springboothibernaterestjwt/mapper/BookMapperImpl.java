package ru.homelab.springboothibernaterestjwt.mapper;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import ru.homelab.springboothibernaterestjwt.dto.BookDto;
import ru.homelab.springboothibernaterestjwt.entity.Book;

@Component
@RequiredArgsConstructor
public class BookMapperImpl implements BookMapper {
    private final ModelMapper modelMapper;

    @Override
    public BookDto mapToBookDto(Book book) {
        return modelMapper.map(book, BookDto.class);
    }

    @Override
    public Book mapToBook(BookDto bookDto) {
        return modelMapper.map(bookDto, Book.class);
    }
}

package ru.homelab.springboothibernaterestjwt.mapper;


import ru.homelab.springboothibernaterestjwt.dto.BookDto;
import ru.homelab.springboothibernaterestjwt.entity.Book;

public interface BookMapper {
    BookDto mapToBookDto(Book book);

    Book mapToBook(BookDto bookDto);
}

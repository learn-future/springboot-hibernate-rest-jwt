package ru.homelab.springboothibernaterestjwt.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.DataException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.homelab.springboothibernaterestjwt.error.BookNotFountException;

@RestControllerAdvice
public class ControllerExceptionApiHandler {
    @Value("${resourceAlreadyExists}")
    private String resourceAlreadyExists;
    @Value("${incorrectData}")
    private String incorrectData;
    @Value("${bookNotFount}")
    private String bookNotFount;

    @ExceptionHandler(BookNotFountException.class)
    public ResponseEntity<String> bookNotFount(BookNotFountException exception) {
        return new ResponseEntity<>(bookNotFount + exception.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<String> resourceAlreadyExists() {
        return new ResponseEntity<>(resourceAlreadyExists, HttpStatus.CONFLICT);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, DataException.class})
    public ResponseEntity<String> incorrectData() {
        return new ResponseEntity<>(incorrectData, HttpStatus.BAD_REQUEST);
    }
}

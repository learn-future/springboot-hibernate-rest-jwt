package ru.homelab.springboothibernaterestjwt.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.homelab.springboothibernaterestjwt.dto.BookDto;
import ru.homelab.springboothibernaterestjwt.facade.BookFacade;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/books")
@RequiredArgsConstructor
public class BookController {
    private final BookFacade bookFacade;

    @GetMapping
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<BookDto>> findAll() {
        return ResponseEntity.ok(bookFacade.findAll());
    }

    @GetMapping("{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public BookDto findById(@PathVariable("id") Long id) {
        return bookFacade.findById(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public BookDto create(@Valid @RequestBody BookDto bookDto) {
        return bookFacade.create(bookDto);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public BookDto update(@PathVariable("id") Long id, @Valid @RequestBody BookDto bookDto) {
        return bookFacade.update(id, bookDto);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    public void delete(@PathVariable("id") Long id) {
        bookFacade.delete(id);
    }

}

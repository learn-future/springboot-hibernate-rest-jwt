package ru.homelab.springboothibernaterestjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootHibernateRestJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootHibernateRestJwtApplication.class, args);
	}

}

-- password user and admin 123456
insert into users(username, email, password)
values ('user', 'user@mail.ru', '$2a$12$ltjpRqpxIABp5SbMHEERnuNWRIBpkgWsQj5VqJpM/PcTv5oDqag4u'),
       ('admin', 'admin@mail.ru', '$2a$12$ltjpRqpxIABp5SbMHEERnuNWRIBpkgWsQj5VqJpM/PcTv5oDqag4u');

insert into users_roles(user_id, role_id)
values (1, 1),
       (2, 3);

create table books
(
    id              bigserial primary key,
    title           varchar(40) not null unique,
    year_of_release int
);

create table authors
(
    id        bigserial primary key,
    firstname varchar(15) not null,
    lastname  varchar(15) not null
);

create table books_authors
(
    book_id   bigint not null,
    author_id bigint not null,
    primary key (book_id, author_id),
    foreign key (book_id) references books (id),
    foreign key (author_id) references authors (id)
);

insert into books(title, year_of_release)
values ('Дубровский', 1841),
       ('Сказка о царе Салтане', 1831),
       ('Двенадцать стульев', 1927);

insert into authors(firstname, lastname)
values ('Александр', 'Пушкин'),
       ('Илья', 'Ильф'),
       ('Евгений', 'Петров');

insert into books_authors(book_id, author_id)
values (1, 1),
       (2, 1),
       (3, 2),
       (3, 3);

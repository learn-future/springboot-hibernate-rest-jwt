create table refresh_token
(
    id         bigserial primary key,
    token      varchar   not null unique,
    expiry_day timestamp not null,
    user_id    bigint,
    foreign key (user_id) references users (id)
);

create table users
(
    id       bigserial primary key,
    username varchar(20)  not null unique,
    email    varchar(50)  not null unique,
    password varchar(120) not null
);

create table roles
(
    id   serial primary key,
    name varchar(20)
);

create table users_roles
(
    user_id bigint not null,
    role_id int    not null,
    primary key (user_id, role_id),
    foreign key (user_id) references users (id),
    foreign key (role_id) references roles (id)
);

insert into roles(name)
values ('ROLE_USER'),
       ('ROLE_MODERATOR'),
       ('ROLE_ADMIN');

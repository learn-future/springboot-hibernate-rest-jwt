package ru.homelab.springboothibernaterestjwt.controller;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.homelab.springboothibernaterestjwt.payload.request.LoginRequest;
import ru.homelab.springboothibernaterestjwt.payload.request.SignupRequest;
import ru.homelab.springboothibernaterestjwt.payload.request.TokenRefreshRequest;
import ru.homelab.springboothibernaterestjwt.security.jwt.JwtUtils;

import javax.annotation.PostConstruct;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AuthControllerTest extends AbstractIntegrationTest {
    @Autowired
    private JwtUtils jwtUtils;
    private String accessToken;
    private String refreshToken;

    @PostConstruct
    private void init() {
        LoginRequest loginRequest = new LoginRequest("user", "123456");
        JsonPath jsonPath = given()
                .basePath("/signin")
                .when()
                .body(loginRequest)
                .post()
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath();

        accessToken = jsonPath.getString("token");
        refreshToken = jsonPath.getString("refreshToken");
    }

    @BeforeAll
    public static void setup() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://localhost:8080/api/auth")
                .setContentType(ContentType.JSON)
                .build();
    }

    @Test
    public void authenticateUser() {
        assertTrue(jwtUtils.validateJwtToken(accessToken));
        assertFalse(refreshToken.isBlank());
    }

    @Test
    public void refreshToken() {
        TokenRefreshRequest tokenRefreshRequest = new TokenRefreshRequest(refreshToken);
        JsonPath jsonPath = given()
                .log().all()
                .basePath("/refreshtoken")
                .when()
                .body(tokenRefreshRequest)
                .post()
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath();

        String accessToken = jsonPath.getString("accessToken");
        String refreshToken = jsonPath.getString("refreshToken");

        assertTrue(jwtUtils.validateJwtToken(accessToken));
        assertFalse(refreshToken.isBlank());

    }

    @Test
    public void registerUser() {
        SignupRequest signupRequest = new SignupRequest(
                "cat",
                "cat@maol.ru",
                Set.of("user"),
                "123456"
        );

        given()
                .log().all()
                .basePath("/signup")
                .when()
                .body(signupRequest)
                .post()
                .then()
                .log().all()
                .statusCode(200)
                .body("message", equalTo("User registered successfully!"));
    }

}

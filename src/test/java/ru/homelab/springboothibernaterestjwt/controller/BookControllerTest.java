package ru.homelab.springboothibernaterestjwt.controller;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import ru.homelab.springboothibernaterestjwt.dto.AuthorDto;
import ru.homelab.springboothibernaterestjwt.dto.BookDto;
import ru.homelab.springboothibernaterestjwt.payload.request.LoginRequest;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class BookControllerTest extends AbstractIntegrationTest {
    private static final String authorization = "Authorization";
    private String tokenUser;
    private String tokenAdmin;

    private String getToken(String username, String password) {
        LoginRequest loginRequest = new LoginRequest(username, password);
        String token = given()
                .when()
                .contentType(ContentType.JSON)
                .body(loginRequest)
                .post("http://localhost:8080/api/auth/signin")
                .then()
                .log().all()
                .statusCode(200)
                .extract()
                .response()
                .jsonPath()
                .getString("token");

        return "Bearer " + token;
    }

    @PostConstruct
    private void init() {
        tokenUser = getToken("user", "123456");
        tokenAdmin = getToken("admin", "123456");
    }

    @BeforeAll
    public static void setup() {
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setBaseUri("http://localhost:8080/api")
                .setBasePath("/books")
                .setContentType(ContentType.JSON)
                .build();
    }

    @Test
    void findAll_thenStatus401() {
        given()
                .when()
                .get()
                .then()
                .statusCode(401);
    }

    @Test
    void findAll_thenStatus200() {
        List<BookDto> books = given()
                .when()
                .header(authorization, tokenUser)
                .get()
                .then()
                .statusCode(200)
                .extract()
                .jsonPath()
                .getList("", BookDto.class);

        Optional<BookDto> optionalBookDto = books.stream()
                .filter(bookDto -> bookDto.getTitle().equals("Дубровский"))
                .findAny();

        assertTrue(optionalBookDto.isPresent());
        assertEquals(optionalBookDto.get().getYear(), 1841);
    }

    @Test
    void findAll_thenStatus404() {
        given()
                .basePath("/book")
                .when()
                .get()
                .then()
                .statusCode(404);
    }

    @Test
    void findById_thenStatus401() {
        given()
                .basePath("/books/1")
                .when()
                .get()
                .then()
                .statusCode(401);
    }

    @Test
    void findById_thenStatus200() {
        given()
                .basePath("/books/1")
                .when()
                .header(authorization, tokenUser)
                .get()
                .then()
                .statusCode(200)
                .body("title", equalTo("Дубровский"))
                .body("year", equalTo(1841))
                .body("authors[0].firstname", equalTo("Александр"))
                .body("authors[0].lastname", equalTo("Пушкин"));
    }

    @Test
    void findById_thenStatus404() {
        given()
                .basePath("/books/22")
                .when()
                .header(authorization, tokenUser)
                .get()
                .then()
                .statusCode(404);
    }

    @Test
    void create_thenStatus401() {
        given()
                .body(new BookDto())
                .when()
                .post()
                .then()
                .statusCode(401);
    }

    @Test
    void create_thenStatus403() {
        AuthorDto authorDto = AuthorDto.builder()
                .firstname("Константин")
                .lastname("Николаев")
                .build();
        BookDto bookDto = BookDto.builder()
                .title("Привет новый мир!")
                .year(2023)
                .authors(Set.of(authorDto))
                .build();

        given()
                .body(bookDto)
                .when()
                .header(authorization, tokenUser)
                .post()
                .then()
                .statusCode(403);
    }

    @Test
    void create_thenStatus201() {
        AuthorDto authorDto = AuthorDto.builder()
                .firstname("Константин")
                .lastname("Николаев")
                .build();
        BookDto bookDto = BookDto.builder()
                .title("Привет новый мир!")
                .year(2023)
                .authors(Set.of(authorDto))
                .build();

        given()
                .body(bookDto)
                .when()
                .header(authorization, tokenAdmin)
                .post()
                .then()
                .statusCode(201)
                .body("title", equalTo("Привет новый мир!"))
                .body("year", equalTo(2023))
                .body("authors[0].firstname", equalTo("Константин"))
                .body("authors[0].lastname", equalTo("Николаев"))
                .log().all();
    }

    @Test
    void create_thenStatus400() {
        given()
                .body(new BookDto())
                .when()
                .header(authorization, tokenAdmin)
                .post()
                .then()
                .assertThat().statusCode(400)
                .log().all();
    }

    @Test
    void create_invalidName_thenStatus400() {
        AuthorDto authorDto = AuthorDto.builder()
                .firstname("слишком длинное имя")
                .lastname("Николаев")
                .build();
        BookDto bookDto = BookDto.builder()
                .title("Новый дивный мир!")
                .year(2023)
                .authors(Set.of(authorDto))
                .build();

        given()
                .body(bookDto)
                .when()
                .header(authorization, tokenAdmin)
                .post()
                .then()
                .statusCode(400)
                .log().all();
    }

    @Test
    void create_thenStatus409() {
        AuthorDto authorDto = AuthorDto.builder()
                .firstname("Константин")
                .lastname("Николаев")
                .build();
        BookDto bookDto = BookDto.builder()
                .title("Дубровский")
                .year(1842)
                .authors(Set.of(authorDto))
                .build();

        given()
                .body(bookDto)
                .when()
                .header(authorization, tokenAdmin)
                .post()
                .then()
                .statusCode(409)
                .log().all();
    }

    @Test
    void update_thenStatus401() {
        given()
                .basePath("/books/3")
                .body(new BookDto())
                .when()
                .put()
                .then()
                .statusCode(401);
    }

    @Test
    void update_thenStatus403() {
        AuthorDto authorDto = AuthorDto.builder()
                .firstname("Катя")
                .lastname("Смешинкова")
                .build();
        BookDto bookDto = BookDto.builder()
                .title("Улыбка!")
                .year(1983)
                .authors(Set.of(authorDto))
                .build();

        given()
                .basePath("/books/3")
                .body(bookDto)
                .when()
                .header(authorization, tokenUser)
                .put()
                .then()
                .statusCode(403);
    }

    @Test
    void update_thenStatus200() {
        AuthorDto authorDto = AuthorDto.builder()
                .firstname("Катя")
                .lastname("Смешинкова")
                .build();
        BookDto bookDto = BookDto.builder()
                .title("Улыбка!")
                .year(1983)
                .authors(Set.of(authorDto))
                .build();

        given()
                .basePath("/books/3")
                .body(bookDto)
                .when()
                .header(authorization, tokenAdmin)
                .put()
                .then()
                .statusCode(200)
                .body("title", equalTo("Улыбка!"))
                .body("year", equalTo(1983))
                .body("authors[0].firstname", equalTo("Катя"))
                .body("authors[0].lastname", equalTo("Смешинкова"))
                .log().all();
    }

    @Test
    void update_thenStatus404() {
        BookDto bookDto = BookDto.builder().title("Привет мир!").build();
        given()
                .basePath("/books/22")
                .body(bookDto)
                .when()
                .header(authorization, tokenAdmin)
                .put()
                .then()
                .statusCode(404);
    }

    @Test
    void update_thenStatus400() {
        given()
                .basePath("/books/2")
                .body(new BookDto())
                .when()
                .header(authorization, tokenAdmin)
                .put()
                .then()
                .statusCode(400);
    }

    @Test
    void delete_thenStatus401() {
        given()
                .basePath("/books/2")
                .when()
                .delete()
                .then()
                .statusCode(401);
    }

    @Test
    void delete_thenStatus403() {
        given()
                .basePath("/books/2")
                .when()
                .header(authorization, tokenUser)
                .delete()
                .then()
                .statusCode(403);
    }

    @Test
    void delete_thenStatus200() {
        given()
                .basePath("/books/2")
                .when()
                .header(authorization, tokenAdmin)
                .delete()
                .then()
                .statusCode(200);
    }

    @Test
    void delete_thenStatus404() {
        given()
                .basePath("/books/23")
                .when()
                .header(authorization, tokenAdmin)
                .delete()
                .then()
                .statusCode(404);
    }
}
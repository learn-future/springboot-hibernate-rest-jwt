FROM eclipse-temurin:17-jre-alpine
ADD /target/springboot-hibernate-rest-jwt-0.0.1-SNAPSHOT.jar backend.jar
ENTRYPOINT ["java", "-jar", "backend.jar"]
